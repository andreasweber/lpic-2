# 200.1 Messen und Problembehandlung bei der Ressourcenverwendung

Links von Frank:

- [Netzwerkverkehr auslesen mit lftop](https://www.linux-community.de/ausgaben/linuxuser/2013/02/netzwerkverkehr-analysieren-mit-iftop/)
- [Programme zum Bandbreiten-Monitoring](https://www.linux-community.de/ausgaben/linuxuser/2013/06/programme-zum-bandbreiten-monitoring/)

## CPU:

### top, htop:

allgemeine Infos, Tasks, CPUs, Interaktion mit Prozessen

Optionen:

- k Kill
- n Ausgabezeilen
- r Renice
- h help
- q quit


### sysstat: Paket, enthält die folgenden Kommandos
#### sar
Sammelt Systeminformationen (Standard: CPU, -n eth: Netzwerk; -b I/O)

```bash
# 4 Werte alle 5 Sekunden
root@andebian1:~# sar 5 4
Linux 4.19.0-17-amd64 (andebian1) 	08.07.2021 	_x86_64_	(2 CPU)

09:19:15        CPU     %user     %nice   %system   %iowait    %steal     %idle
09:19:20        all      0,00      0,00      0,00      0,00      0,00    100,00
09:19:25        all      0,10      0,00      0,10      0,00      0,00     99,80
09:19:30        all      0,00      0,00      0,30      0,00      0,00     99,70
09:19:35        all      0,00      0,00      0,10      0,00      0,00     99,90
Durchschn.:     all      0,03      0,00      0,13      0,00      0,00     99,85
```

Parameter:

- -n DEV 5 4 Informationen Netzwerk
- -b 5 4 Information zum Input/Output
- -A zeigt als Demon erfasste Daten an

 
#### iostat: Zeigt CPU- und I/O-Statistiken

```bash
root@andebian1:~# iostat
Linux 4.19.0-17-amd64 (andebian1) 	08.07.2021 	_x86_64_	(2 CPU)

avg-cpu:  %user   %nice %system %iowait  %steal   %idle
           0,01    0,00    0,13    0,00    0,00   99,86

Device             tps    kB_read/s    kB_wrtn/s    kB_read    kB_wrtn
fd0               0,00         0,00         0,00          8          0
sda               0,12         1,07         1,62     273189     413364
```

Parameter:

- -c CPU-Verwendung
- -d Datenträger
- -h Human Readable
- -x Extended (Erweiterte Statistiken)

Links:

- [Sysstat Tutorial](http://sebastien.godard.pagesperso-orange.fr/tutorial.html)

### vmstat
Analysetool für:

- Prozesse
- Speicher
- I/O
- System
- CPU

```bash
root@andebian1:~# vmstat
procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu-----
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 0  0      0 3447316  70184 311944    0    0     1     1    1   10  0  0 100  0  0
```

Parameter:
- -a Aktiver Speicher
- 2 5 Fünf Werte in zwei Sekunden
- -d Festplatte
- -D Zusammenfassung Festplattenstatistiken
- -s Ereigniszähler/Speicher
- -f Forks seit reboot

## Prozesse:
  ps, iostat, iotop, top, htop, vmstat, pstree, pgrep

### pstree 
Zeigt die laufenden Prozesse in einer Baumansicht, Debianpaket: `pstree` 
```bash
root@andebian1:~# pstree -pn
systemd(1)─┬─systemd-udevd(273)
           ├─systemd-timesyn(419)───{systemd-timesyn}(452)
           ├─dhclient(441)
           ├─rsyslogd(451)─┬─{rsyslogd}(469)
           │               ├─{rsyslogd}(470)
           │               └─{rsyslogd}(471)
           ├─cron(454)
           ├─dbus-daemon(457)
           ├─systemd-logind(464)
           ├─sshd(478)───sshd(5487)───sshd(5500)───bash(5501)───su(5504)───bash(5505)───pstree(5741)
           ├─agetty(482)
           ├─systemd-journal(1563)
           └─systemd(5490)───(sd-pam)(5491)
```

Parameter:

- -a Kommandozeilenparameter anzeigen
- -G Ausgabe im VT100-Terminal-Modus
- -p PIDs anzeigen
- -n nach PIDs sortieren
- (-u Besitzer anzeigen)

### ps
Zeigt Informationen über laufende Prozesse an

- -a mit einem Terminal verknüpfte Prozesse aller Benutzer
- -u Zeigt mehr Infos an (Startzeit, Pfad, Benutzer)
- -x Prozesse, die nicht mit einem Terminal verbunden sind
- -C zeigt alle Instanzen eines Prozesses
- -U [username] Zeigt alle Prozesse des Benutzers
- (-f zeigt Pfadabhängigkeiten)

```bash
root@andebian1:~# ps aux
USER        PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root          1  0.0  0.2 170716 10520 ?        Ss   Jul05   0:03 /sbin/init
root          2  0.0  0.0      0     0 ?        S    Jul05   0:00 [kthreadd]
root          3  0.0  0.0      0     0 ?        I<   Jul05   0:00 [rcu_gp]
root          4  0.0  0.0      0     0 ?        I<   Jul05   0:00 [rcu_par_gp]
root          6  0.0  0.0      0     0 ?        I<   Jul05   0:00 [kworker/0:0H-kblockd]
root          8  0.0  0.0      0     0 ?        I<   Jul05   0:00 [mm_percpu_wq]
root          9  0.0  0.0      0     0 ?        S    Jul05   0:00 [ksoftirqd/0]
root         10  0.0  0.0      0     0 ?        I    Jul05   0:00 [rcu_sched]
...
```
### iotop
Zeit wie top/htop eine Prozessliste an, gibt aber Informationen über Datenträger-I/O aus.

Parameter:

- -o nur aktive Prozesse
- -b nicht-interaktiv
- -n [nummer] Anzahl Wiederholungen
- -d [abstand] Abstand zwischen Wiederholungen
- -u [user] Nach Benutzer filtern


```bash
root@andebian1:~# iotop -bn1
Total DISK READ:         0.00 B/s | Total DISK WRITE:         0.00 B/s
Current DISK READ:       0.00 B/s | Current DISK WRITE:       0.00 B/s
   TID  PRIO  USER     DISK READ  DISK WRITE  SWAPIN      IO    COMMAND
     1 be/4 root        0.00 B/s    0.00 B/s  0.00 %  0.00 % init
     2 be/4 root        0.00 B/s    0.00 B/s  0.00 %  0.00 % [kthreadd]
     3 be/0 root        0.00 B/s    0.00 B/s  0.00 %  0.00 % [rcu_gp]
     4 be/0 root        0.00 B/s    0.00 B/s  0.00 %  0.00 % [rcu_par_gp]
     6 be/0 root        0.00 B/s    0.00 B/s  0.00 %  0.00 % [kworker/0:0H-kblockd]
...
```
### uptime / w
Zeigen Informationen zur Systemauslastung und über die angemeldeten Benutzer (nur w)

```bash
root@andebian1:~# uptime
 11:23:08 up 3 days,  1:05,  1 user,  load average: 0,00, 0,00, 0,00
```

```bash
root@andebian1:~# w
 11:23:22 up 3 days,  1:05,  1 user,  load average: 0,00, 0,00, 0,00
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
andreas  pts/0    192.168.1.180    09:18    1.00s  0.12s  0.01s sshd: andreas [priv]
```

### vermutlich nicht prüfungsrelevant, aber trotzdem interessant

- who
- whowatch

## Netzwerk

### netstat / ss
Zeigen Netzwerkverbindungen und Routinginformationen

Parameter:

- -n numerische Anzeige
- -l Sockets, die im Status LISTEN sind
- -p zeigt Prozesse an
- -r zeigt Routing-Tabelle an
- -i Liste von Interfaces
- -a alle Verbindungen
- -c wie watch

### iptraf
Interaktives Tool, welches die zur Zeit aktiven Netzwerkverbindungen anzeigt.

### lsof
Zeigt offene Dateien an (also auch Sockets)

Parameter:
- -i nur Netzwerksockets
- -n numeric
- -P Portnummern nicht zu Portnamen auflösen

### vermutlich nicht prüfungsrelevant, aber trotzdem interessant

- iftop: visualisiert die Netzwerkauslastung bei verschiedenen Verbindungen
- speedometer: visualisiert den aktuellen Datendurchsatz (entweder pro Interface oder pro Datei)
- bmon: gibt grafisch oder numerisch detaillierte Infos zum Datendurchsatz aus
- ntop: webbasiertes Analysetool des Netzwerkverkehrs
- acpi: Kernel-Schnittstelle für Power-Management
- lm-sensors: Tool, welches Hardwaredaten (Temperatur, Lüfter-Drehzahl etc.) ausliest
- cpufreq-info: interagiert mit dem Kernel und gibt Informationen über den Leistungs-Governor aus

# 200.2 Prognostizieren zukünftiger Ressourcenanforderungen


## collectd
Unix-Dämon, der lokal läuft und Netzwerkleistungsdaten sammelt. *Read-Plugins* sammeln Daten, *Write-Plugins* geben diese Daten als RRD oder CSV aus.

## Cacti
Software, die periodisch Messwerte sammelt und stellt diese Daten mittels RRDtool über Webinterface dar.

## nagios
Softwaretool zum Monitoring komplexer IT-Infrastrukturen. Enthält Lösungen zur Alarmierung per SMS/E-Mail, Telefon o. Ä. Systeme können passiv per SNMP oder aktiv per Plugin überwacht werden.

## icinga2
Ursprünglich Nagios-Fork, wurde aber deutlich weiterentwickelt.

## nicht im Buch erwähnt, aber dennoch nützlich

### munin
Software zur Überwachung von Rechnern im Netzwerk, Master sammelt Leistungsdaten von Nodes. Webschnittstelle visualisiert diese Daten mittels RRDtool.

### zabbix
Netzwerk-Monitoring-System, welches Daten passiv per SNMP oder aktiv über einen Agent erfasst

