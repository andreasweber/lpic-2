# NFS (Network File System)

*NFS* ist ein Protokoll, um unter UNIX entfernte Dateisysteme zur Verfügung zu stellen bzw. einzubinden. Kommt heutzutage in den Versionen **NFSv3** und **NFSv4** vor. Unter **NFSv3** erfolgt die Authorisierung auf Host-Ebene, bei **NFSv4** ist auch eine Authorisizerung auf Benutzerebene (über Kerberos) möglich.

Server-Konfigurationsdatei: `/etc/exports`

```
/verzeichnis   rechner(rw,no_root_squash) [rechner2(optionen) ...]
/verzeichnis2	rechner*.domain(optionen)
```

Optionen:

- **root_squash**, **no_root_squash**: Root wird auf nobody gemappt
- **all_squash**: Alle Benutzer werden auf nobody gemappt
- **ro**, **rw** read only, read write
- **sync**, **async** Bei sync antwortet der Server erst, wenn alles geschrieben wurde
- **fsid** Entweder UID, oder root bzw. 0

Mit `exportfs -a` werden alle Verzeichnisse in der `/etc/exports` bei laufendem Server exportiert. `exportfs -o rw client-rechner:/pfad` wird das Verzeichnis `/pfad` für den Rechner `client-rechner` freigegeben.

Auf clientseite können freigegebene Ordner regulär gemounted werden:

```bash
root@andebian1:~# mount -t nfs4 10.100.27.75:/test /mnt/nfs/
root@andebian1:~#
```

## Tools

### showmount

Fragt den mountd an, um Informationen über den NFS-Server zu bekommen.

Optionen:

- `-a`, `-all`	Client (Hostname und IP)
- `-d`, `--directory`	gemountete Verzeichnisse
- `-e`, `--exports`		zeigt alle Exports

### rpcinfo

Tool, um auf dem Host angebotene RPC-Dienste anzuzeigen

Optionen:

- `-p` probe: Zeige alle RPC-Dienst

# Samba

Stellt Windows-Dateifreigabe und Domänenfunktionen zur Verfügung

Konfigurationsdatei: `/etc/samba/smb.conf`. Konfigurationsdatei kann mit `testparm` überprüft werden.

## smbpasswd

Samba-Benutzer werden mit `smbpasswd` verwaltet. Mit `smbpasswd -a user` lässt sich ein Samba-Passwort für einen Unix-User anlegen, `smbpasswd user` ändert das Passwort für einen existierenden Benutzer.

## smbstatus

Zeigt eine Übersicht über den Server und die verbundenen Benutzer an:

```bash
root@andebian2:/etc/samba# smbstatus

Samba version 4.9.5-Debian
PID     Username     Group        Machine                                   Protocol Version  Encryption           Signing
----------------------------------------------------------------------------------------------------------------------------------------
14056   sambatest    sambatest    10.100.27.75 (ipv4:10.100.27.75:42280)    SMB3_11           -                    partial(AES-128-CMAC)
14123   sambatest    sambatest    10.100.27.6 (ipv4:10.100.27.6:49732)      SMB3_11           -                    partial(AES-128-CMAC)

Service      pid     Machine       Connected at                     Encryption   Signing
---------------------------------------------------------------------------------------------
lpic         14123   10.100.27.6   Thu Jul 29 01:15:06 PM 2021 CEST -            -
lpic         14056   10.100.27.75  Thu Jul 29 12:06:26 PM 2021 CEST -            -
sambatest    14123   10.100.27.6   Thu Jul 29 01:26:12 PM 2021 CEST -            -

Locked files:
Pid          Uid        DenyMode   Access      R/W        Oplock           SharePath   Name   Time
--------------------------------------------------------------------------------------------------
14123        1001       DENY_NONE  0x100081    RDONLY     NONE             /lpic   .   Thu Jul 29 13:21:35 2021
14123        1001       DENY_NONE  0x100081    RDONLY     NONE             /home/sambatest   .   Thu Jul 29 13:26:12 2021
14123        1001       DENY_NONE  0x100081    RDONLY     NONE             /home/sambatest   .   Thu Jul 29 13:26:12 2021
```