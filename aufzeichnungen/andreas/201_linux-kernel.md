# 201.3 Der Linux-Kernel

> **Achtung** Diese Datei ist unvollständig und enthält nur die Nachträge.

## DKMS

**DKMS** wird verwendet, um bei der Aktualisierung des Kernels zu überprüfen, ob zusätzlich installierte Kernelmodule (wie externe Treiber) aktualisiert werden müssen.

Aktionen:

- add fügt Kernelmodul zur Laufzeit hinzu
- remove entfernt Kernelmodul zur Laufzeit

Parameter:

- -m Name des Moduls
- -v Version des Moduls
- -k Version des Kernel

## Kernel(-module) zur Laufzeit verwalten

### lsmod
Listet die geladenen Kernelmodule und deren Abhängigkeiten auf

### modinfo [Modulname]
Gibt Informationen zu einem bestimmten Kernelmodul aus

Parameter:

- -a nur Autor
- -d nur Beschreibung
- -l nur Lizenz
- -p nur Parameter, die dem Modul übergeben wurden
- -n nur Dateiname

### insmod [Modulpfad]

> **Achtung:** Kompletter Modulpfad erforderlich: `/lib/modules/...`

Module zur Laufzeit hinzufügen. Abhängigkeiten werden geprüft, aber nicht aufgelöst (vgl. modprobe)

### rmmod [Modulname]

Entfernt ein Modul, ohne die Abhängigkeiten aufzulösen.

- -f force
- -v verbose

### modprobe
Fügt Kernelmodule zur Laufzeit hinzu und entfernt diese und kann Abhängigkeiten automatisch auflösen.

Parameter:

- -f force
- -n dry-run
- -r entfernt Modul
- -v verbose

### depmod
Legt unter Anderem die Abhängigkeiten zwischen den Modulen in der Datein `modules.dep` ab.

Parameter:

- -n dry-run
- -A quick scant vorher, ob es neuere Module als in der `modules.dep` gibt.

### Konfigurationsdatei
Die Konfiguration, welche Kernelmodule beim Systemstart geladen werden, lag in 2.4er-Kerneln in `/etc/modules.conf`. Mittlerweile liegen diese Informationen in `/etc/modprobe.d/`.

Beispielsweise lassen sich auf diese Weise Aliase anlegen:

```bash
alias eth0 e100
```

## Informationen über das System sammeln

### lsusb
Zeigt Informationen über verbundene USB-Devices an.

Parameter:

- -v [bus]:[device] Zeigt nur das angegebene Device
- -d [vendor]:[product] Zeigt nur das angegebene Device

### lspci
Zeigt Informationen über die an den PCI-Bussen verbundenen Devices an.

- -b Bus-zentrierte Ansicht: Fragt den Kernel nicht ab. **Prüfungsfrage!!**
- -d [vendor]:[device] Zeigt nur das angegebene Device

### lsdev
Zeigt einen Überblick über die verwendete Hardware, bezieht seine Daten aus dem `/proc`-Dateisystem

### sysctl
Updated Kernel-Parameter zur Laufzeit. Beispiel IP-Forwarding. Konfigurationsdatei, um die Änderungen persistent zu machen: `/etc/sysctl.conf`.

Beispiel:
```bash
root@andebian1:~# sysctl -w net.ipv4.ip_forward=1
net.ipv4.ip_forward = 1
```


## udev

Läuft im Userspace und verwaltet die Gerätedateien in `/dev/`. Die Konfigurationsdatei liegt in `/etc/udev/udev.conf`, Regelsätze liegen in `/etc/udev/rules.d`. Nach Änderungen in diesem Verzeichnis muss der Dämon neu gestartet werden. 

### Überwachung von udev

Prüfungsrelevant, aber veraltet, ist das Tool `udevmonitor`. Mittlerweile wird dir Monitoring-Fähigkeit von `udevadm monitor` angeboten. Udevadm unterstützt weitere Kommandos (info, trigger usw., siehe man-page).