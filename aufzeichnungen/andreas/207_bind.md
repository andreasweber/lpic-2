# Grundsätzliche Einrichtung

Ohne weitere Konfiguration löst bind Anfragen *iterativ* auf.

- iterative Namensauflösung: Der Server beantwortet die Anfrage mit einer Liste von autoritativen Nameservern, die der entsprechende Nameserver weitergibt.
- rekursive Namensauflösung: Der Server gibt die Anfrage an einen anderen Nameserver weiter, wartet auf die Antwort und gibt diese an die anfragende Seite zurück.

Konfigurationsdateien:

> **Wichtig** Die Konfigurationsdatei muss nach Änderungen neu eingelesen werden.

- `/etc/named.conf`: Haupt-Konfigurationsdatei
- `/var/named/named.ca`: Root-Server

Wichtige Tools:

- `rdnc` Bind-Interaktions-Tool
  - `reload` lädt die Config neu
  - `halt` beendet den Dämon sofort
  - `stop` beendet den Dämon und schließt Schreibvorgänge ab
  - `freeze` stoppt Schreibvorgänge
  - `thaw` startet Schreibvorgänge wieder
  
