# Programme aus dem Quellcode übersetzen und installieren

Programme lassen sich meistens als gepackte TAR-Archive herunterladen. Der Installationsprozess passiert in einem Dreischritt:

- **./configure** Das meistens vorhandene configure-Script durchsucht das System nach Abhängigkeiten, überprüft die Vollständigkeit der Toolchain und legt das Makefile an **Prüfungsrelevant: Hierfür wird das Makefile.in verwendet.
- **make** Startet den Kompilier-Vorgang anhand des Makefile
- **make install** Installiert die Binaries, Grafiken und man-Pages. **Prüfungsrelevant: Muss als root ausgeführt werden**

> **TODO** tar, gzip etc

# Datensicherung

## Zu sichernde Verzeichnisse

### Dringend sichern

- `/etc`
- `/home`
- `/root`
- `/var` bei Mail- und Webservern

## sollte man sichern

- `/bin`
- `/boot`
- `/lib`
- `/opt`
- `/sbin`
- `/usr`

## nicht sichern

- `/dev`
- `/mnt`
- `/proc`
- `/tmp`

# Verschiedene Bandlaufwerke:

> Sind heutzutage vermutlich nicht mehr relevant, können aber Prüfungsrelevant sein

- */dev/st0* (SCSI, rückspulend)
- */dev/nst0* (SCSI, nicht rückspulend)
- */dev/ft0* (Floppy, rückspulend)
- */dev/nf0* (Floppy, nicht rückspulend)

# Backup-Tools

## dd
Liest blockweise Daten ein und gibt diese wieder aus. Kann Gerätedateien und Filesystem-Dateien lesen und schreiben.

### Beispiel

```bash
# MBR sichern
root@andebian1:~# dd if=/dev/sda of=mbr.backup ibs=512 count=1
1+0 records in
1+0 records out
512 bytes copied, 0.000129002 s, 4.0 MB/s
```

Parameter:

- if input file
- of output file
- bs Block Size (überschreibt ibs und obs)
- ibs input block size
- obs output block size
- cbs conversion buffer size


## tar
Verwaltet Archive (auf Tapes oder im Dateisystem) und (de-)kompremiert diese optional

Parameter:

c compress
x extract
t table (anzeigen)
z gzip
j bzip2
J xz
v verbose
w interaktiver Modus
f file (Muss am Ende stehen)

## mt
(magnetic tape) steuert Tapes

Operations:

- eom end of media (zum Anhängen)
- rewind spult zum Anfang zurück
- f file (Gerätedatei)

## verschiedene Backup-Tools

- bacula
- amanda
- backuppc

## rsync
Tool, um Daten über das Netzwerk zu sichern. Im Normalbetrieb werden Größe und Zeitstempel verglichen. Wenn bei der Quelle ein abschließender Backslash angegeben wird, wird er Verzeichnisinhalt synchronisiert, wenn nicht, wird das Verzeichnis synchronisiert.

Parameter:

- -a Archive (behinhaltet u. A. -r und -p)
- -z Kompression
- -r rekursiv
- -p preserve Permissions
- -v verbose
- -u update (neuere Dateien am Ziel werden ausgelassen)
- -4 verwendet IPv4
- -6 verwendet IPv6
- -e verwende remote Shell wie ssh
- --delete löscht Dateien am Ziel, wenn sie in der Quelle nicht existieren


# Benutzer über systembezogene Angelegenheiten benachrichtigen

## /etc/issue, /etc/issue.net, /etc/motd

- issue: vor der Anmeldung
- issue.net: vor der Netzwerkanmeldung
- motd: nach der Anmeldung

Platzhalter:

- `\n` Hostname
- `\o` Domainname
- `\l` TTY
- `\s` Betriebssystem
- `\m` Architektur
- `\r` Release des Kernels
- `\v` Betriebssystemversion
- `\d` Datum
- `\t` Zeit
- `\u` Anzahl Benutzer
- `\U` Zeichenkette "[n] User(s)"

## Kommandos:

- `wall`
- `shutdown -k`
- `systemctl poweroff` (Benachtichtigung wird mit `--no-wall` verhindert)