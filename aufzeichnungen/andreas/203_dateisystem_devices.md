# Linux-Dateisysteme

## mount, umount

`mount` hängt Dateisysteme in den Verzeichnisbaum ein, `umount` hängt das Verzeichnis wieder aus.

Parameter:

- -a mountet alle Dateisysteme in der `/etc/fstab`, die nicht die Option *noauto* haben.
- -r read-only
- -w writeable
- -t Dateisystemtyp
- -v verbose
- -o [Mount-Optionen]

Folgende Mount-Optionen sind möglich (als Parameter von `-o` oder in der `/etc/fstab`:

- auto: Dateisystem wird autmatisch gemountet
- noauto: Das Dateisystem wird nicht automatisch gemountet.
- usrquota: Quota für Benutzer
- grpquota: Quota für Gruppen
- suid: ermöglicht SUID-Bits
- nosuid: verhindert SUID-Bits
- exec: ermöglicht das Ausführen von Dateien
- noexec: verhindert das Ausführen von Dateien
- remount: Hängt ein bereits eingehängtes Dateisystem erneut ein
- ro: read only
- rw: read/write
- user: normale Benutzer können das Dateisystem einhängen. Es kann nur vom gleichen Benutzer wieder ausgehängt werden
- nouser: normale Benutzer können das Dateisystem nicht einhängen
- users: wie `user`, aber alle Benutzer können das Dateisystem umounten
- username=[username]
- password=[password]
- defaults enthält `rw, suid, dev, exec, auto, nouser, async`
- loop: Nutzt das Loopback-Device zum mounten, wenn Images aus dem Dateisystem gemountet werden (**Prüfungsfrage**): 

```
mount -o loop -t iso9660 cdrom.iso /mnt/cdrom
```

## fstab
Die Datei `/etc/fstab` wird für die Verwaltung von Dateisystemen verwendet.

Die Datei enthält 6 Spalten. **Prüfungsrelevant!**

1. Einzuhängendes Filesystem
2. Mountpoint
3. Dateisystemtyp
4. Optionen
5. Einstellungen für das Sicherungsprogramm `dump` (0: wird nicht gesichert)
6. Einstellung für `fsck` beim Systemstart (0 wird nicht geprüft, 1 wird priorisiert geprüft, 2 wird danach geprüft)

## mtab, /proc/self/mounts
Die Datei `/etc/mtab` bzw. `/proc/self/mounts` enthält die aktuell eingehängten Dateisysteme.

## sync
Der Befehl `sync` schreibt alle noch im Schreibcache vorgehaltenen Daten auf die Datenträger.

## Paging (Swap)
Die Befehle `swapopn` bzw. `swapoff` stellen dem Kernel Swap-Dateisysteme zum auslagern zur Verfügung.

Parameter:

- -a: aktiviert alle Swap-Dateisysteme in der `/etc/fstab`
- -L [label] spricht Dateisysteme über ein Label an
- -U [UUID] spricht Dateisysteme über die UUID an
- -s zeigt die gerade verwendeten Swap-Dateisysteme.

## UUID
UUIDs sind einmalige Zeichenketten, die Dateisysteme identifizieren können. Heutzutage werden sie oft in der `/etc/fstab` gespeichert. UUIDs können mit dem Kommando `blkid` angezeigt werden

```bash
root@andebian1:~# blkid
/dev/sda1: UUID="74e5b95a-2c00-46a4-b4be-f85c46fbdb4e" TYPE="ext4" PARTUUID="9d9e2ec1-01"
/dev/sda2: UUID="d5c32330-b4ba-4a25-bd4f-32f0be9659f6" TYPE="ext4" PARTUUID="9d9e2ec1-02"
/dev/sda3: UUID="7dfe59ee-ace1-4a11-ad82-1c1325ebcdef" TYPE="ext4" PARTUUID="9d9e2ec1-03"
/dev/sda5: UUID="d79e26c8-4f8a-4211-97a9-416dbf75d9f3" TYPE="swap" PARTUUID="9d9e2ec1-05"
/dev/sda6: UUID="3d56b6c3-d634-412f-a19e-a74ab6dc1c7e" TYPE="ext4" PARTUUID="9d9e2ec1-06"
```

Die Gerätedateien werden im Verzeichnis `/dev/disk/by-uuid` verlinkt und können so angesprochen werden (**Prüfungsrelevant!**)

## systemd
SystemD kann sich um das Mounten von Dateisystemen kümmern. Dies hat unter Anderem den Vorteil, das Abhängigkeiten beachtet werden können. systemd wandelt die Enträge in der `/etc/fstab` in temporäre mount units in `/run/systemd/generator` um.

## fsck
Frontend zur Überprüfung von Dateisystemen. Die Überprüfung sollte bei nicht gemounteten Dateisystemen durchgeführt werden.

Parameter:

- -f force
- -A alle Dateisysteme in `/etc/fstab`
- -t [typ] lädt das entsprechende Backend-Tool
- -c sucht defekte Blöcke
- -b [block] gibt einen anderen Superblock an
- -y antwortet auf alle Fragen mit *yes*

Shutdown kennt Parameter, die den Dateisystemcheck beim Neustart beeinflussen (**Prüfungsfrage!**):

- -f unterbindet den Check beim nächsten Systemstart
- -F erzwingt den Check beim nächsten Systemstart

## ext-Dateisysteme

### tune2fs
Tool zum Bearbeiten von Dateisystemeinstellungen wie beispielsweise das Prüfintervall mit `fsck`

- -c Anzahl Mounts vor `fsck`
- -C überschreibt die Mount-Anzahl
- -j legt ein Journal an und konvertiert so ein ext2- in ein ext3-Dateisystem
- -i Intervall in Tagen
- -T Überschreibt den Check-Zeitpunkt
- -U ändert die UUID

### debugfs
Tool, um ext-Dateisysteme zu untersuchen oder Dateien wiederherzustellen. Die Dateisysteme dürfen nicht gemounted sein. `debugfs` unterstützt eine Reihe interaktiver Kommandos, um mit dem Dateisystem zu interagieren:

- **lsdel** zeigt gelöschte inodes
- **dump** stellt Inode-Inhalt in einer Datei wieder her

### mke2fs, mkfs
Erstellt ein ext-Dateisystem, siehe mkfs

Parameter:

- -t Dateisystemtyp
- -c Prüft das Gerät
- -v verbose
- -j Journal (Erstellt ein ext3-Dateisystem)
- -L gibt das Label an

## xfs
Dateisystem, welches Journals auf externen Datenträgern erlaubt. Die `xfsprogs` beginnen mit xfs_, `xfsdump` und `xfsrestore` nicht. 

### mkfs.xfs

Legt ein XFS-Dateisystem an. In diesem Beispiel wird das Journal auf einer externen Partition gespeichert.

```bash
mkfs.xfs /dev/sda1 -j logdev=/dev/sdb1
```

### xfs_info
Ist ein Tool, das Metadaten ausgibt:

```bash
root@andebian1:~# xfs_info image.xfs
meta-data=image.xfs              isize=512    agcount=1, agsize=5120 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=1, sparse=1, rmapbt=0
         =                       reflink=0
data     =                       bsize=4096   blocks=5120, imaxpct=25
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0, ftype=1
log      =Internes Protokoll     bsize=4096   blocks=855, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =keine                  extsz=4096   blocks=0, rtextents=0
```

### xfs_check
Überprüft ein nicht gemountetes xfs-Dateisystem. `-l ` gibt den Pfad des Journals an.

> **Anmerkung:** Wurde durch `xfs_repair -n` ersetzt, könnte aber noch prüfungsrelevant sein.

### xfs_repair
Repariert ein xfs-Dateisystem.

Parameter:

- -n Überprüft das Dateisystem ohne Reparatur (ehemals `xfs_check`)
- -l [logdev] Gibt das Device an, in dem das Journal gespeichert wird

### xfsdump / xfsrestore
Tools, die XFS-Dateisysteme sichern und wiederherstellen können. `xfsdump` unterstützt inkrementelle Sicherungen. (Trace ist ein verbosity Level)

```bash
xfsdump -v trace -f /media/backup/sicherung /dev/[xfsdisk]
xfsrestore -f /media/backup/sicherung /dev/[xfsdisk]
```

## zfs
Fortschrittliches Dateisystem, welches in den Eigenschaften btrfs ähnelt, für die Prüfung aber nicht weiter relevant ist.

## btrfs
Neues Dateisystem, basiert auf btree. Dateien können bis 16 EiB groß sein. Bestehende ext- und ReiserFS-Dateisysteme können konvertiert werden.

- Snapshots
- dynamische inode-Tabelle
- SSD-aware und -optimiert
- Copy-on-Write (Kopien werden zunächst virtuell angelegt und erst real erstellt, wenn sie verwendet werden.)
- fsck im laufenden Betrieb
- Kompression
- RAID-Unterstützung auf Dateisystem-Ebene
- Subvolumes

### btrfs-convert
Konvertiert Dateisysteme in btrfs. Der Parameter `-r` macht diese Änderung rückgängig.

## ISO-Dateisysteme

### mkisofs
Tool, das ISO9660-Dateisysteme erstellt. Heißt in Debian 10 `genisoimage` (In Debian 11 existiert ein Symlink). Rock Ridge und Joilet sind Erweiterungen, die gleichzeitig verwendet werden können.

Parameter:

- -r rock ridge extension: Unix-Berechtigungen können im Dateisystem gespeichert werden
- -l lange Dateinamen
- -ldots erlaubt Dateinamen, die mit einem Punkt beginnen
- -o output file
- -J Joilet-Format (für Windows)
- -udf UDF-Dateisystem (z.B. für DVDs)

### cdrecord / wodim
Tools, um CDs oder DVDs zu brennen. 

## Partitionierung mit fdisk
Tool, um Volumes zu partitionieren. Die Partitionstabelle wird mit `fdisk -l` ausgegeben:

```bash
root@andebian1:~# fdisk -l /dev/sda
Disk /dev/sda: 20 GiB, 21474836480 bytes, 41943040 sectors
Disk model: Virtual Disk
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: dos
Disk identifier: 0x9d9e2ec1

Device     Boot    Start      End  Sectors  Size Id Type
/dev/sda1  *        2048  9764863  9762816  4,7G 83 Linux
/dev/sda2        9764864 11718655  1953792  954M 83 Linux
/dev/sda3       11718656 21483519  9764864  4,7G 83 Linux
/dev/sda4       21485566 41940991 20455426  9,8G  5 Extended
/dev/sda5       21485568 37107711 15622144  7,5G 82 Linux swap / Solaris
/dev/sda6       37109760 41940991  4831232  2,3G 83 Linux
```

fdisk wird mit interaktiven Befehlen gesteuert:

- *a* activate: Markiert Partitionen als bootbar (für Windows)
- *d* delete: Löschen einer Partition
- *l* list: Anzeigen der unterstützten Partitionstypen
- *m* menu: Hilfe
- *n* new: Neue Partition
- *p* print: Anzeige der Partitionstabelle
- *q* quit: Beendet fdisk, ohne zu Schreiben
- *t* type: Setzt den Partitionstyp (siehe *l*)
- *u* units: Ändert die Anzeigeeinheiten
- *v* verify: überprüft die Partitionstabelle
- *w* write: Schreibt die Partitionstabelle
- *x* eXtra: Zeigt weitere Optionen

gängige Partitionstypen:

- *7* HPFS/Windows NTFS
- *c* Windows 95 FAT
- *f* Windows 95 FAT (erweiterte Partition)
- *82* Linux Swap
- *83* Linux
- *85* Linux (erweiterte Partition)
- *8e* Linux LVM

## S.M.A.R.T.
Bezeichnet eine Technologie, bei der Festplatten sich selbst überwachen, um Datenträgerdefekte zu erkennen. Unter Linux werden die `smartmontools` verwendet.

### smartctl
Tool, um über die S.M.A.R.T.-Schnittstelle mit Festplatten zu interagieren

Parameter:

- -a alle Smart-Parameter
- -a alle Parameter
- -l [logtyp] Gibt das Log zurück
- -t [long,short...] stößt einen Self-Test an
- --scan gibt alle S.M.A.R.T.-Fähigen Devices aus

### smartd
Dämon, der im Hintergrund mit S.M.A.R.T interagiert und automatisch Tests anstoßen und Fehler melden kann. Konfigurationsdateien: `/etc/default/smartmontools` und `/etc/smartd.conf`.

## Automatisches Mounten mit autofs
autofs kann automatisch Dateisysteme mounten, wenn auf die mountpoints zugegriffen wird und nach einer bestimmten Zeit wieder unmounten. Die Haupt-Konfigurationsdatei ist `/etc/auto.master`. In dieser Datei wird auf Map-Dateien verwiesen, in der die Dateisysteme referenziert werden (Beispiel: `/etc/auto.misc`).

> **Prüfungsrelevant:** Änderungen in der `/etc/auto.master` benötigen einen Neustart des Service, Änderungen in den Map-Dateien bemerkt autofs selbst.