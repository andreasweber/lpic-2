# Software-RAIDS
*Redundant Array of Independent (Inexpensive) Disks*.

## Gebräuchliche RAID-Level

- RAID 0: Striping

  Keine Redundanz, Geschwindigkeitsvorteile, voller Speicher nutzbar (mindestens 2 Platten)

- RAID 1: Mirroring

  Alle Blocks werden gespiegelt, halber Speicher nutzbar, keine Geschwindigkeitsvorteile (mindestens 2 Platten)

- RAID 5: Verteilung mit Parity-Daten

  Leichter Performance-Zuwachs, System toleriert einen Plattenausfall, n-1 Platten nutzbar (mindestens 3 Platten)

- RAID 10: Kombination aus 1 und 0

  Auf unterer Ebene zwei Raid 1, Verbunden als Raid 0. Geschwindigkeitsvorteil mit Redundanz. n/2 Platten nutzbar (mindestens 4 Platten)

## RAID unter Linux

Die beteiligten Partitionen müssen den Typ `fd (Linux raid autodetect)` haben.

Die Konfigurationsdatei ist `/etc/mdadm.conf`. Der Befehl zum Anlegen eines RAID lautet wiefolgt:

```bash
root@andebian1:~# mdadm --create --verbose /dev/md0 --auto=yes --level=0 --raid-devices=2 /dev/sdb1 /dev/sdc1
mdadm: chunk size defaults to 512K
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md0 started.
```

Informationen über das Raid finden sich in der `/proc/mdstat`:

```bash
root@andebian1:~# cat /proc/mdstat
Personalities : [raid0]
md0 : active raid0 sdc1[1] sdb1[0]
      20951040 blocks super 1.2 512k chunks

unused devices: <none>
```

Detailliertere Informationen liefert `mdadm`:

```bash
root@andebian1:~# mdadm --detail /dev/md0
/dev/md0:
           Version : 1.2
     Creation Time : Thu Jul 15 09:18:07 2021
        Raid Level : raid0
        Array Size : 20951040 (19.98 GiB 21.45 GB)
      Raid Devices : 2
     Total Devices : 2
       Persistence : Superblock is persistent

       Update Time : Thu Jul 15 09:18:07 2021
             State : clean
    Active Devices : 2
   Working Devices : 2
    Failed Devices : 0
     Spare Devices : 0

        Chunk Size : 512K

Consistency Policy : none

              Name : andebian1:0  (local to host andebian1)
              UUID : d4431801:af404d30:f2f62156:153de584
            Events : 0

    Number   Major   Minor   RaidDevice State
       0       8       17        0      active sync   /dev/sdb1
       1       8       33        1      active sync   /dev/sdc1
```

Ein RAID-Array kann auf die folgende Weise erweitert werden:

```bash
# Festplatte als Spare hinzufügen
mdadm --add /dev/md0 /dev/sdXY

# Array vergrößern
mdadm --grow --raid-devices=[anzahl] /dev/md0

# Dateisystem vergrößern
resize2fs /dev/md0
```

# hdparm
Tool, um Festplatteninformationen und -einstellungen auszulesen und zu verändern.

Parameter:

- -v Übersicht
- -d DMA-Modus lesen/schreiben
- -X MDMA/SDMA-Einstellung lesen/schreiben
- -i Metainformationen vom Kernel abfragen
- -I Metainformationen direkt von der Festplatte abfragen
- -o Read-ahead-Konfiguration lesen/schreiben
- -B[num] Advanced Power Management. Niedrige Zahl: Hohe Ersparnis, hohe Zahl: hohe Performance, 255: APM aus
- -g Festplattengeometrie
- -r Read only flag lesen/schreiben
- -z Partitionstabelle neu lesen

# tune2fs
Siehe Dateisysteme

Parameter (hardwarenahe)

- e [continue,remount-ro,panic] Gibt das Verhalten im Falle von Fehlern an
- -m [reserved Blocks percentage] Reservierte Blöcke in Prozent
- -r [reserved Blocks] absolute Anzahl der reservierten Blöcke
- -u [user] Benutzer, der die reservierten Blöcke verwenden darf
- -g [group] Gruppe, die die reservierten Blöcke verwenden darf
- s [0,1] de/aktiviert die Spare-Superblocks

# SSD und NVMe

Altuelle SSDs werden entweder über den *AHCI-Controller* mit dem PCIe-Controller verbunden, abhilfe bietet die moderne NVMe-Technologie, die einen direkten Anschluss von SSDs an den PCIe-Controller ermöglicht. Das entsprechende Linux-Kommando `nvme`. Zum Freigeben von Speicherplatz auf SSDs kann `fstrim` verwendet werden (Auf alten Platten nicht empfohlen).

# Logical Volume Manager

Abstraktionsebene, um **Physical Volumes** zu **Volume Groups** genannten Speicherpools zusammezufassen, die dann logisch in **Logical Volumes** aufgeteilt werden können. Die beteiligten Partitionen sollen den Partitionstyp `8e (Linux LVM)` haben.

## Physical Volumes

- pvscan Zeigt eine Liste aller Physical Volumes
- pvdisplay Zeigt metainformationen über die Physical Volumes
- pvcreate Erstellt ein Physical Volume auf einer Partition

## Volume Group

- vgscan Sucht nach Volume Groups
- vgdisplay Zeigt Metainformationen zu den existierenden Volume Groups an
- vgcreate [name] [devices] Legt eine Volume Group an
- vgextend [vg] [pv] Fügt ein Physical Volume zur Volume Group hinzu
- vgreduce [vg] [pv] Entfernt ein Physical Volume aus der Volume Group

## Logical Volume

- lvcreate -n [name] -L [size] [volume_group] 
- lvextend -L [size] [logical_volume] Erweitert das Logical Volume (Anschließend Dateisystem vergrößern, z. B. mit `resize2fs`)
- lvreduce -L [size] [logical_volume] Verkleinert ein Logical Volume
- lvremove [logical_volume]

## LVM Snapshots

Snapshots halten eine Momentaufnahme fest. Sie enthalten nur die Änderungen seit dem Snapshot und können daher entsprechend kleiner sein.

Snapshots werden als Logical Volumes mit `lvcreate -s` erstellt:

```bash
root@andebian1:~# lvcreate -L 8G -s -n lv_part1_s1 /dev/vg_andi/lv_part1
  Logical volume "lv_part1_s1" created.
```